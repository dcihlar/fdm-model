# FDM model simulation

`rack.py` can be used to simulate various conditions.

## Limits

Note that there is some distortion in high frequency carrier (second mixer stage) due to low resolution.

Increasing resolution by reducing `td` in `timebase.py` can decrease or even eliminate that distortion.
But the simulation time will rise up significantly.

So, with default `timebase.py` it is normal to hear every channel on every other channel, but way below the normal volume.
