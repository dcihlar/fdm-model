import struct

def save(data, fname):
	max_data = max(data)
	min_data = min(data)

	scaled_data = (data - min_data) * (16383. / (max_data - min_data))
	scaled_data = [int(round(e)) for e in scaled_data]

	with open(fname + '.raf', 'wb') as f:
		for val in scaled_data:
			f.write(struct.pack('<H', val))
