import gzip
import os

from scipy.io import wavfile
from scipy import signal
from numpy import append

from timebase import *


def _get_max10(num):
	max10 = 1
	while True:
		if num % (max10 * 10) == 0:
			max10 *= 10
		else:
			break
	return max10

def load(fname):
	fname = os.path.join('sound', fname + '.wav.gz')
	with gzip.open(fname, 'rb') as f:
		samplerate, data = wavfile.read(f)


	max10 = _get_max10(samplerate)
	mult = (1/td) // max10
	div = samplerate // max10
	sig = signal.resample_poly(data, mult, div)[:N] / 32768
	if len(sig) < N:
		sig = append(sig, [0]*(N - len(sig)))
	return sig

def save(sig, fname):
	if not os.path.isdir('out'):
		os.mkdir('out')
	fname = os.path.join('out', fname + '.wav')
	samplerate = 44100
	max10 = _get_max10(samplerate)
	mult = samplerate // max10
	div = (1/td) // max10
	data = signal.resample_poly(sig, mult, div)
	wavfile.write(fname, samplerate, data)

def test():
	print('save')
	from numpy import pi, sin
	sig = sin(2 * pi * 1000 * t)
	save(sig, 'test')

	print('load')
	sig = load('test')

	print('plot')
	import matplotlib.pyplot as plt
	plt.figure()
	plt.plot(t, sig)
	plt.show()

if __name__ == '__main__':
	test()
