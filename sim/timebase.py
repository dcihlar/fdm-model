from scipy import arange

print('init timebase')
T = 1.
td = 1e-6
nyq = 2 * td
t = arange(0, T, td)
N = len(t)

bandwidth = 4000
if_multiplier = 8
if_frequency = bandwidth * if_multiplier
ch0_multiplier = 24
