from scipy.signal import butter, lfilter

from timebase import *


def _filter(make_filter):
	b, a = make_filter()
	return lambda sig: lfilter(b, a, sig)

def _make_if_filter():
	low = if_frequency
	high = if_frequency + bandwidth
	return butter(2, [low * nyq, high * nyq], btype='band')
intermediate = _filter(_make_if_filter)

def _make_audio_filter():
	return butter(4, 3000 * nyq)
audio = _filter(_make_audio_filter)

def _make_ifc_filter():
	if_freq = if_multiplier * bandwidth
	return butter(4, if_freq * nyq)
#ifc = _filter(_make_ifc_filter)
ifc = lambda data: data

def _make_pll_filter():
	max_pll_freq = (ch0_multiplier - if_multiplier + 11) * bandwidth
	return butter(4, max_pll_freq * nyq)
pll = _filter(_make_pll_filter)

def _make_amp_filter():
	low = ch0_multiplier * bandwidth
	high = (ch0_multiplier + 11) * bandwidth
	return butter(4, [low * nyq, high * nyq], btype='band')
amp = _filter(_make_amp_filter)


def plot_test(ax, make_filter, title, max_freq=100000):
	from scipy.signal import freqz
	from numpy import pi

	b, a = make_filter()
	w, h = freqz(b, a, worN=2000)
	ax.plot(w / (nyq * pi * 1000.), abs(h))
	ax.set_xlabel('Frequency (kHz)')
	ax.set_ylabel('Gain')
	ax.set_title(title)
	ax.axis([0, max_freq / 1000., 0, 1.1])
	ax.grid()

def test():
	import matplotlib.pyplot as plt

	print('graphing filter')
	fig = plt.figure()
	plot_test(fig.add_subplot(2, 2, 1), _make_if_filter, 'IF', max_freq=50000)
	plot_test(fig.add_subplot(2, 2, 2), _make_audio_filter, 'audio', max_freq=16000)
	plot_test(fig.add_subplot(2, 2, 3), _make_pll_filter, 'PLL', max_freq=300000)
	plot_test(fig.add_subplot(2, 2, 4), _make_amp_filter, 'amplifier', max_freq=300000)
	plt.subplots_adjust(hspace=0.5, wspace=0.3)
	plt.show()


if __name__ == '__main__':
	test()
