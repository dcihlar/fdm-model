#!/usr/bin/env python3

print('hello')
import os.path

import matplotlib.pyplot as plt
from numpy import pi, sin, linspace, arange
from scipy import fft

from timebase import *
import module
import raf
import wav


print('simulate modules')
print('    transmitter')
osc = module.oscillator()
pll1 = module.pll(osc, 2)
pll2 = module.pll(osc, 6)
pll3 = module.pll(osc, 12)

mod = 0

sig = 0.1*wav.load('num1')
mod += module.modulator(sig, osc, pll1)

sig = 0.1*wav.load('num2')
mod += module.modulator(sig, osc, pll2)

sig = 0.1*wav.load('num3')
mod += module.modulator(sig, osc, pll3)

mod = module.amplifier(mod / 3, level = 0.1)

print('    generating RAF')
raf.save(mod, 'mod-all')


print('    receiver')
osc = module.oscillator(error = -0.1)
pll1 = module.pll(osc, 4)
pll2 = module.pll(osc, 5)
pll3 = module.pll(osc, 6)
ch = {
	pll1.channel: module.demodulator(mod, osc, pll1),
	pll2.channel: module.demodulator(mod, osc, pll2),
	pll3.channel: module.demodulator(mod, osc, pll3),
}

print('save samples')
for chid, chi in ch.items():
	wav.save(chi, 'test-ch%d' % chid)


print('fft')
yf = fft(mod)
yf = 2./N * abs(yf[0:N//2])
xf = linspace(0., 1./(2*td), N//2)


print('plot')
fig = plt.figure()
ax1 = plt.subplot2grid((3, 2), (0, 0), fig=fig)
ax1.set_title('Modulated signal')
ax1.plot(t, mod)
ax1.grid()
ax2 = plt.subplot2grid((3, 2), (1, 0), rowspan=2, fig=fig)
ax2.set_title('FFT')
ax2.plot(xf/bandwidth-(ch0_multiplier-1), yf)
ax2.set_xlim(0, 13)
ax2.set_xticks(arange(0, 13, 1))
ax2.grid(axis='x')
for rowid, (chid, chi) in enumerate(ch.items()):
	ax = plt.subplot2grid((3, 2), (rowid, 1), fig=fig)
	ax.set_title('CH%d' % chid)
	ax.plot(t, chi)
	ax.set_ylim(-0.2, 0.2)
	ax.grid()
plt.subplots_adjust(hspace=0.5, wspace=0.3)
plt.savefig(os.path.join('out', 'fig.png'), dpi=300)
plt.show()
