from scipy import signal
from numpy import pi, sin

from mixer import mix
import sigfilter
from timebase import *


def modulator(sig, osc, pll):
	sig = sigfilter.audio(sig)
	sig = mix(sig, osc.if_carrier)
	sig = sigfilter.intermediate(sig)
	sig = mix(sig, pll.ch_carrier)
	return sig

def demodulator(sig, osc, pll):
	sig = mix(sig, pll.ch_carrier)
	sig = sigfilter.intermediate(sig)
	sig = mix(sig, osc.if_carrier)
	sig = sigfilter.audio(sig)
	return sig

class oscillator():
	def __init__(self, error=0., pure=False):
		self.base_frequency = bandwidth + error
		self.if_frequency = if_multiplier * self.base_frequency
		func = sin if pure else signal.square
		carrier = 0.1 * func(2 * pi * self.if_frequency * t)
		self.if_carrier = sigfilter.ifc(carrier)

class pll():
	def __init__(self, osc, ch, pure=False):
		if ch not in range(1, 13):
			raise ValueError('Invalid channel')
		ch_freq = (ch0_multiplier - if_multiplier + ch - 1) * osc.base_frequency
		func = sin if pure else signal.square
		carrier = 0.1 * func(2 * pi * ch_freq * t)
		self.ch_carrier = sigfilter.pll(carrier)
		self.channel = ch

def amplifier(sig, level=1.):
	return sigfilter.amp(sig) * level
