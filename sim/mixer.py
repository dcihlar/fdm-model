# experiment with the real SA612:
#       in: 100mV p2p, 1kHz
#  carrier: 250mV p2p, 16kHz
#      out: 750mV p2p, sum
#			40mV, 1kHz
#			140mV, 15kHz
#			140mV, 17kHz
#			almost no carrier

def mix(sig, carrier):
	return 0.8*sig + 48*(carrier * sig)


def test():
	print('preparations')
	import matplotlib.pyplot as plt
	from numpy import pi, sin, arange, linspace
	from scipy import fft

	T = 0.01
	td = 1e-6
	t = arange(0, T, td)
	N = len(t)

	print('mix')
	sig = 0.05 * sin(2 * pi * 1000 * t)
	carrier = 0.125 * sin(2 * pi * 16000 * t)
	out = mix(sig, carrier)

	print('fft')
	yf = fft(out)
	yf = 2./N * abs(yf[0:N//2])
	xf = linspace(0., 1./(2*td), N//2)

	print('plot')
	fig = plt.figure()
	ax1 = fig.add_subplot(211)
	ax1.set_title('mixed signal')
	ax1.plot(t, out)
	ax1.axis([0, 0.004, -.5, .5])
	ax1.grid()
	ax2 = fig.add_subplot(212)
	ax2.set_title('fft')
	ax2.plot(xf, yf)
	ax2.axis([0, 20e3, 0, 1.])
	ax2.grid()
	plt.show()

if __name__ == '__main__':
	test()
