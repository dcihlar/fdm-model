use <../components/connectors.scad>
use <../components/pcb.scad>
use <../basic/material.scad>
use <../basic/screw.scad>

//FIXME: this is copy&paste from rack.scad!!!
connection_depth = headerMaleAngledMountToTop() - 2.54 + headerFemaleHeight();

module cardHolderGuide(width, clearance, height_mm, depth_mm, slot_depth, detailed)
{
    slot_width = 2;
    ofs = (2.54 + pcb_thick()) / 2;    // pcb offset from raster
    width_mm = width * 2.54 - clearance * 2;
    
    material("acrylic")
    difference()
    {
        translate([-width_mm/2, 0, 0])
        cube([width_mm, depth_mm, height_mm]);
        
        // slots
        if (detailed)
        {
            for (i = [-width/2:width/2-1])
            {
                translate([i * 2.54 - slot_width / 2 + ofs, -1, height_mm - slot_depth])
                cube([slot_width, depth_mm + 2, slot_depth + 1]);
            }
        }
    }
}

module cardHolderHorizontal(width, depth=10, guide_height=3, slot_depth=1, detailed=true)
{
    width_mm = (width + 2) * 2.54;
    height_mm = 2.54 * 2 - 0.1;
    profile_thick = 1;
    depth_mm = depth * 2.54 + connection_depth;
    profile_length = depth_mm + profile_thick;
    lprofile_width = 5;
    guide_width_mm = width_mm - lprofile_width;
    screw_ofs = lprofile_width / 2;
    plateau_width = width_mm - lprofile_width * 2;
    
    // T profile
    material("aluminum")
    translate([width_mm/2 - screw_ofs, profile_length/2 - profile_thick, 0])
    difference()
    {
        cube([width_mm, profile_length, height_mm], center=true);
        
        cutout_height = (height_mm + profile_thick) / 2;
        for (i = [-1, 1])
        {
            translate([0, profile_thick, i*cutout_height])
            cube([width_mm+2, profile_length, height_mm], center=true);
        }
        
        cutout_ofs = (width_mm - lprofile_width + 1) / 2;
        for (i = [-1, 1])
        {
            translate([i * cutout_ofs, profile_thick, 0])
            cube([lprofile_width + 1, profile_length, height_mm], center=true);
        }
    }
    
    // guides
    for (i = [0, 1])
    {
        translate([width_mm / 2 - 2.54, 0, 0])
        mirror([0, 0, i])
        translate([0, 0, profile_thick / 2])
        cardHolderGuide(width, screw_ofs, guide_height, depth_mm, slot_depth, detailed=detailed);
    }
    
    // screws
    for (ofs = [0, width_mm-2.54*2])
    {
        translate([ofs, -profile_thick, 0])
        rotate([-90, 0, 0])
        flatScrew(2.5, 4, detailed=detailed);
    }
}

module cardHolder(depth, type="horizontal", detailed=true)
{
    if (type == "horizontal")
    {
        cardHolderHorizontal(depth, detailed=detailed);
    }
    else if (type == "vertical")
    {
        cardHolderVertical(depth, detailed=detailed);
    }
}


// example
union()
{
    $fs = 0.5;
    cardHolder(10, type="horizontal");
}
