use <../components/connectors.scad>
use <../components/pcb.scad>
use <../basic/material.scad>
use <../basic/screw.scad>

function card_bottom_offset() = pcb_thick() + 2.54 / 2;

module cardPlateHolder(height, drill=2.5)
{
    pin_height = 3;
    profile_height = pin_height + height;
    profile_width = 6;
    profile_thick = 1;
    profile_middle = profile_width / 2;
    drill_height = pin_height + height / 2;
    
    translate([-profile_thick/2, 0, -pin_height])
    {
        // the holder itself
        material("brass")
        difference()
        {
            cube([profile_width, profile_width, profile_height]);
            translate([profile_thick, profile_thick, -1])
            cube([profile_width-profile_thick*2, profile_width, profile_height+2]);
            
            // milling for pins
            translate([-1, -1, -1])
            cube([profile_width+2, profile_thick+1.01, pin_height+1]);
            translate([-1, profile_thick+1, -1])
            cube([profile_width+2, 3, pin_height+1]);
            
            // drill
            translate([profile_middle, profile_thick+1, drill_height])
            rotate([90, 0, 0])
            cylinder(profile_thick+2, d=drill);
        }
        
        // possible screw
        if ($children > 0)
        {
            translate([profile_middle, 0, drill_height])
            rotate([-90, 0, 0])
            for (i = [0:$children-1])
            {
                children(i);
            }
        }
    }
}

module card(width, height, depth, rail_space=0, detailed=true)
{
    pcb_width = width - rail_space * 2;
    height_mm = height * 2.54;
    depth_mm = depth * 2.54;
    width_mm = pcb_width * 2.54;
    panel_width_mm = width * 2.54;
    panel_spacing = 0.1;
    base_to_conn = pcb_thick() + 2.54 / 2;
    bottom_space = 2.54 * 2 - base_to_conn;
    
    // PCB
    PCB(pcb_width, depth, type="dot", detailed=detailed);
    
    // connector
    PCBPlace(pcb_width-1, 1, rot=180)
    headerMaleAngled(pcb_width-1);
    
    // front plate holders
    holder_height = height_mm - (pcb_thick() + bottom_space) * 2;
    PCBPlace(3, depth, rot=180)
    cardPlateHolder(holder_height)
    translate([0, 0, -pcb_thick()])
    thumbScrew(2.5, 4, detailed=detailed);
    
    PCBPlace(pcb_width-1, depth, rot=180)
    cardPlateHolder(holder_height)
    translate([0, 0, -pcb_thick()])
    flatScrew(2.5, 4, detailed=detailed);
    
    // front plate
    material("white pcb")
    translate([-rail_space*2.54 + panel_spacing/2, depth_mm, -bottom_space+panel_spacing/2])
    cube([panel_width_mm-panel_spacing, pcb_thick(), height_mm-panel_spacing]);
}


// example
union()
{
    $fs=0.1;
    card(12, 5, 10);
}
