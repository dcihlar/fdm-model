use <../components/pcb.scad>
use <../basic/material.scad>
use <../basic/screw.scad>

module blindPlate(width, height=2, detailed=true)
{
    spacing = 0.1;
    width_mm = (width + 2) * 2.54;
    height_mm = height * 2.54;
    screw_ofs = 2.54;
    
    material("white pcb")
    translate([-screw_ofs, 0, -screw_ofs])
    translate([spacing/2, 0, spacing/2])
    cube([width_mm - spacing, pcb_thick(), height_mm - spacing]);
    
    for (y = [0:((height > 3)?1:0)])
    {
        for (x = [0, width * 2.54])
        {
            translate([x, pcb_thick(), y * (height_mm - 2.54*2)])
            rotate([90, 0, 0])
            flatScrew(2.5, 4, detailed=detailed);
        }
    }
}


// examples
union()
{
    $fs=0.5;
    translate([0,  0, 0]) blindPlate(10);
    translate([0, 10, 0]) blindPlate(10, 6);
}
