use <../components/connectors.scad>
use <../components/pcb.scad>
use <blindplate.scad>
use <backplane.scad>
use <lprofile.scad>
use <holder.scad>
use <card.scad>

connection_depth = headerMaleAngledMountToTop() - 2.54 + headerFemaleHeight();

module rackPillars(width, height, depth)
{
    width_mm = width * 2.54;
    height_mm = height * 2.54;
    depth_mm = depth * 2.54;
    
    lprofile_height = height / 2;
    lprofile_right = lprofile_hole_offset() * 2 + width_mm;
    lprofile_front = depth_mm + connection_depth;
    
    lprofile(lprofile_height);
    
    translate([lprofile_right, 0, height_mm])
    rotate([0, 180, 0])
    lprofile(lprofile_height);
    
    translate([lprofile_right, lprofile_front, 0])
    rotate([0, 0, 180])
    lprofile(lprofile_height);
    
    translate([0, lprofile_front, height_mm])
    rotate([0, 180, 180])
    lprofile(lprofile_height);
}

module rack(width, height, depth)
{
    rackPillars(width, height, depth);
}

module rackPlace(ofs)
{
    translate([lprofile_hole_offset(), 0, lprofile_hole_position(ofs)])
    for (i = [0:$children-1])
    {
        children(i);
    }
}

module rackPlaceRelative(rel_ofs)
{
    translate([0, 0, rel_ofs * 2.54 * 2])
    for (i = [0:$children-1])
    {
        children(i);
    }
}

module rackPlaceFront(depth, rel_ofs = 0)
{
    rackPlaceRelative(rel_ofs)
    translate([0, depth * 2.54 + connection_depth, 0])
    for (i = [0:$children-1])
    {
        children(i);
    }
}

module rackPlaceBackplane(rel_ofs = 0)
{
    rackPlaceRelative(rel_ofs)
    translate([-2.54, -pcb_thick(), 2.54])
    rotate([-90, 0, 0])
    for (i = [0:$children-1])
    {
        children(i);
    }
}

module rackPlaceCard(wofs = 0, hofs = 0, spacing = 0, orientation="horizontal")
{
    y_rot = (orientation == "vertical") ? -90 : 0;
    z_spacing = (orientation == "horizontal") ? spacing : 0;
    x_spacing = (orientation == "vertical") ? spacing : 0;
    
    translate([wofs * 2.54, 0, hofs * 2.54])
    rotate([0, y_rot, 0])
    translate([-2.54, connection_depth, -card_bottom_offset()])
    for (i = [0:$children-1])
    {
        translate([0, 0, -spacing * i * 2.54])
        children(i);
    }
}


// example
union()
{
    $fs = 1;
    $fa = 30;
    vcard_height = 4;
    vcard_width = 10;
    hcard_height = 4;
    width = vcard_height * 4 + 2;
    hcard_width = width - 2;
    depth = 10;
    detailed = false;
    
    rack(width, 30, depth);
    
    rackPlace(3)
    {
        rackPlaceBackplane()
        {
            backplane(width, 6, detailed=detailed);
            for (i = [0:2-1])
            {
                PCBPlace(4, 2 + i * hcard_height)
                headerFemale(hcard_width-3);
            }
        }
        
        rackPlaceCard(wofs=3, hofs=-1, spacing=hcard_height, orientation="horizontal")
        {
            card(hcard_width, hcard_height, depth, 1, detailed=detailed);
            card(hcard_width, hcard_height, depth, 1, detailed=detailed);
        }
    }
    
    rackPlace(9)
    {
        rackPlaceBackplane()
        {
            backplane(width, vcard_width-2, detailed=detailed);
            for (i = [0:4-1])
            {
                PCBPlace(4 + i * vcard_height, 1, rot=90)
                headerFemale(vcard_width - 1);
            }
        }
        
        rackPlaceRelative(-5)
        {
            cardHolder(width, depth, type="horizontal", detailed=detailed);
            rackPlaceFront(depth)
            blindPlate(width, 2, detailed=detailed);
        }
        
        rackPlaceCard(wofs=3, hofs=-8, spacing=vcard_height, orientation="vertical")
        {
            card(vcard_width, vcard_height, depth, detailed=detailed);
            card(vcard_width, vcard_height, depth, detailed=detailed);
            card(vcard_width, vcard_height, depth, detailed=detailed);
            card(vcard_width, vcard_height, depth, detailed=detailed);
        }
    }
}
