use <../basic/material.scad>

profile_size = 6;
profile_width = 1;
    
function lprofile_hole_offset() = profile_width + (profile_size - profile_width) / 2;

function lprofile_hole_position(n, cut_offset=2.54, hole_distance=5.08) = cut_offset + n * hole_distance;

module lprofile(size, cut_offset=2.54, hole_distance=5.08, hole_size=2.5)
{
    height = hole_distance * size;
    
    material("brass")
    difference()
    {
        // create the profile
        cube([profile_size, profile_size, height]);
        translate([profile_width, profile_width, -1])
        cube([profile_size,
              profile_size,
              height + 2]);
        
        // create screw holes
        for (i = [0:size-1])
        {
            translate([lprofile_hole_offset(), profile_width + 1, lprofile_hole_position(i, cut_offset, hole_distance)])
            rotate([90, 0, 0])
            cylinder(profile_width + 2, d = hole_size);
        }
    }
}


// example
union()
{
    $fs=0.5;
    lprofile(3);
}
