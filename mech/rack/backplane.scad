use <../components/pcb.scad>
use <../basic/screw.scad>

module backplane(width, height, screw_size=2.5, screw_length=4, detailed=true)
{
    // PCB
    PCB(width+2, height+2, "strip", detailed=detailed);
    
    // mount screws
    for (x = [1,width+2-1])
    {
        for (y = [1,height+2-1])
        {
            PCBPlace(x, y, "bottom")
            rotate([180, 0, 0])
            flatScrew(screw_size, screw_length, detailed=detailed);
        }
    }
}


// example
union()
{
    $fs = 0.5;
    backplane(10, 10);
}
