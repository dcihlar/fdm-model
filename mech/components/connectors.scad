use <../basic/material.scad>

/**** header parameters ****
 * note: MountToTop is from the soldered point to the
 *       point where the header mates.
 */

function headerFemaleHeight() = 7;
function headerProfiFemaleHeight() = 4.2;
function headerMaleAngledMountToTop() = 4.04;
function headerProfiMaleAngledMountToTop() = 4.5;

module headerPlastic(height)
{
    spacing = 0.05;
    
    material("black plastic")
    translate([0, 0, height/2])
    intersection()
    {
        translate([0, 0, -spacing / 2])
        cube([2.54, 2.54, height - spacing], center=true);
        translate([0, 0, -1])
        rotate([0, 0, 45])
        cube([3, 3, height+2], center=true);
    }
}

module headerProfiBase(height)
{
    headerPlastic(height);
    material("brass")
    translate([0, 0, -0.01])
    cylinder(height+0.02, d=1.8);
}

module headerGenerator(n)
{
    for (i = [0:n-1])
    {
        translate([i * 2.54, 0, 0])
        {
            for (c = [0:$children-1])
            {
                children(c);
            }
        }
    }
}

module headerMale(n)
{
    pin_above = 6;
    pin_below = 3.1;
    plastic_height = 2.54;
    pin_height = pin_above + pin_below + plastic_height;
    pin_width = 0.635;
    
    headerGenerator(n)
    {
        headerPlastic(plastic_height);
        material("brass")
        translate([0, 0, pin_height/2-pin_below])
        cube([pin_width, pin_width, pin_height], center=true);
    }
}

module headerProfiMale(n)
{
    pin_above = 3.96;
    pin_above_w = 0.47;
    plastic_height = 4;
    pin_below = 3;
    pin_below_w = 0.64;
    
    headerGenerator(n)
    {
        headerProfiBase(plastic_height);
        material("brass")
        {
            translate([0, 0, plastic_height-0.1])
            cylinder(pin_above+0.1, d=pin_above_w);
            translate([0, 0, -pin_below])
            cylinder(pin_below+0.1, d=pin_below_w);
        }
    }
}

module headerFemaleGeneric(n, pin_above)
{
    pin_below = 3;
    
    headerGenerator(n)
    {
        difference()
        {
            headerProfiBase(pin_above);
            translate([0, 0, 0.1])
            cylinder(pin_above, d=1);
        }
        material("brass")
        translate([0, 0, -pin_below])
        cylinder(pin_below+0.05, d=0.5);
    }
}

module headerFemale(n)
{
    headerFemaleGeneric(n, headerFemaleHeight());
}

module headerProfiFemale(n)
{
    headerFemaleGeneric(n, headerProfiFemaleHeight());
}

module headerMaleAngled(n)
{
    pin_below = 3;
    pin_above = 6;
    pin_vertical = pin_below + 2.54/2;
    pin_width = 0.635;
    mount_to_top = headerMaleAngledMountToTop();
    pin_horizont = pin_above + mount_to_top;
    plastic_height = 2.54;
    
    headerGenerator(n)
    {
        material("brass")
        translate([0, 0, pin_vertical/2-pin_below])
        cube([pin_width, pin_width, pin_vertical], center=true);
        
        translate([0, mount_to_top, 2.54/2])
        rotate([-90, 0, 0])
        union()
        {
            translate([0, 0, -plastic_height])
            headerPlastic(plastic_height);
            
            material("brass")
            translate([0, 0, -pin_horizont/2 + pin_above])
            cube([pin_width, pin_width, pin_horizont], center=true);
        }
    }
}

module headerProfiMaleAngled(n)
{
    mount_to_top = headerProfiMaleAngledMountToTop();
    pin_above = 4;
    pin_above_w = 0.47;
    pin_below = 3.2;
    pin_below_w = 0.6;
    plastic_height = 2.95;

    headerGenerator(n)
    {
        material("brass")
        translate([0, 0, -pin_below])
        cylinder(pin_below+2.54/2, d=pin_below_w);
        
        translate([0, mount_to_top, 2.54/2])
        rotate([-90, 0, 0])
        {
            translate([0, 0, -plastic_height])
            headerProfiBase(plastic_height);
            
            material("brass")
            {
                cylinder(pin_above, d=pin_above_w);
                translate([0, 0, -mount_to_top])
                cylinder(mount_to_top-0.1, d=pin_below_w);
            }
        }
    }
}

// examples
union()
{
    $fs=0.5;
    translate([0,   0, 0]) headerMale(4);
    translate([0,   4, 0]) headerProfiMale(4);
    translate([0,   8, 0]) headerFemale(4);
    translate([0,  12, 0]) headerProfiFemale(4);
    translate([0,  16, 0]) headerMaleAngled(4);
    translate([0,  32, 0]) headerProfiMaleAngled(4);
}
