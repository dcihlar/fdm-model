use <../basic/material.scad>

shaft_hex_d = 2.0*1.155;

module shaftCA9_9005()
{
    shaft_hex_len = 3.9;
    shaft_small_len = 1;
    shaft_small_d = 3;
    shaft_len = 10;
    shaft_d = 5;
    
    material("black plastic")
    {
        translate([0, 0, -shaft_hex_len])
        cylinder(shaft_hex_len, d=shaft_hex_d, $fn=6);
        cylinder(shaft_small_len, d=shaft_small_d);
        translate([0, 0, shaft_small_len])
        cylinder(shaft_len, d=shaft_d);
    }
}

module shaftCA9_9067()
{
    shaft_hex_len=6.3;
    shaft_len=4.5;
    shaft_d=6;
    
    material("black plastic")
    {
        translate([0, 0, -shaft_hex_len])
        cylinder(shaft_hex_len, d=shaft_hex_d, $fn=6);
        cylinder(shaft_len, d=shaft_d);
    }
}

module potCA9(shaft_ofs=0)
{
    body_w = 9.8;
    body_h = 10;
    body_d = 4.8;
    bot_to_shaft = 5.3;
    pin_fat_h = 0.3;
    pin_fat_w = 2;
    pin_len = 3.5;
    pin_w = 1;
    pin_thick = 0.3;
    pin_space = 3.8;
    center_w = 3;
    front_to_pins = (body_d - pin_space) / 2;
    
    // pins
    material("copper")
    for (i = [0:2])
    {
        y = [0, pin_space, 0];
        translate([i * 2.54 - pin_w/2, y[i] - pin_thick/2, -pin_len])
        {
            cube([pin_w, pin_thick, pin_len+1]);
            
            translate([-(pin_fat_w-pin_w)/2, 0, pin_len])
            cube([pin_fat_w, pin_thick, pin_fat_h+1]);
        }
    }
    
    // body
    translate([2.54, body_d/2-front_to_pins, bot_to_shaft])
    {
        material("blue plastic")
        difference()
        {
            cube([body_w, body_d, body_h], center=true);
            rotate([90, 0, 0])
            cylinder(body_d+2, d=center_w, center=true);
        }
        material("white plastic")
        rotate([90, 0, 0])
        difference()
        {
            cylinder(body_d, d=center_w, center=true);
            cylinder(body_d+2, d=shaft_hex_d, center=true, $fn=6);
        }
    }
    
    // shaft
    if ($children > 0)
    {
        translate([2.54, -front_to_pins-shaft_ofs, bot_to_shaft])
        rotate([90, 0, 0])
        children(0);
    }
}


// example
union()
{
    $fs=0.5;
    potCA9(shaft_ofs=2)
    shaftCA9_9067();
}
