use <../basic/material.scad>

module pot3310C(knob_distance=2)
{
    shaft_len = 5.59;   // part of it is covered with bushing
    shaft_dia = 3.18;
    front_to_pins = 3.91;
    pin_dia = 0.51;
    pin_len = 5.97;
    body_h = 9.53;
    body_w = 9.53;
    body_d = 5.18;
    bot_to_shaft = 4.83;
    standoff_h = 0.38;
    standoff_w = 0.51;
    bushing_len = 0.51;
    bushing_dia = 7.62;
    knob_len = 12.7;
    knob_dia = 12.7;
    knob_hat = 1;
    
    // body
    material("blue plastic")
    translate([-body_w/2+2.54, -front_to_pins, 0])
    difference()
    {
        union()
        {
            cube([body_w, body_d, body_h]);
            translate([body_w/2, 1, bot_to_shaft])
            rotate([90, 0, 0])
            cylinder(bushing_len+1, d=bushing_dia);
        }
        translate([standoff_w, -1, -1])
        cube([body_w-standoff_w*2,body_d+2,standoff_h+1]);
    }
    
    // pins
    for (i = [0:2])
    {
        translate([i * 2.54, 0, -pin_len])
        cylinder(pin_len+1, d=pin_dia);
    }
    
    // shaft and knob
    translate([2.54, -front_to_pins, bot_to_shaft])
    rotate([90, 0, 0])
    {
        // shaft
        material("blue plastic")
        translate([0, 0, -1])
        cylinder(shaft_len+1, d=shaft_dia);
    
        // knob
        if (knob_distance > 0 && knob_distance < shaft_len)
        {
            material("black plastic")
            translate([0, 0, knob_distance])
            {
                cylinder(knob_len-knob_hat, d=knob_dia);
                cylinder(knob_len, d=knob_dia-knob_hat*2);
            }
        }
    }
}


// example
union()
{
    $fs = 0.2;
    pot3310C();
}
