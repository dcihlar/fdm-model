use <../basic/material.scad>

pcb_thick = 1.6;

function pcb_thick() = pcb_thick;

module PCBPlace(x, y, layer="top", rot=0)
{
    translate([x * 2.54, y * 2.54, 0])
    rotate([0, 0, rot])
    if (layer == "top")
    {
        translate([0, 0, pcb_thick])
        for (i = [0:$children-1])
            children(i);
    }
    else if (layer == "bottom")
    {
        rotate([180, 0, 0])
        for (i = [0:$children-1])
            children(i);
    }
}

module PCB(width, height, type="dot", spacing=0.1, detailed=true)
{
    width_mm = width * 2.54;
    height_mm = height * 2.54;
    drill = 1;
    copper_width = 0.036;
    trace_width = 1.5;
    
    difference()
    {
        // PCB
        union()
        {
            // FR4
            material("green pcb")
            translate([spacing / 2, spacing / 2, 0])
            cube([width_mm - spacing, height_mm - spacing, pcb_thick]);
            
            // copper
            material("copper")
            if (detailed && type == "dot")
            {
                for (x = [1:width-1])
                    for (y = [1:height-1])
                        PCBPlace(x, y, "bottom")
                        cylinder(copper_width, d=trace_width);
            }
            else if (detailed && type == "strip")
            {
                for (y = [1:height-1])
                    PCBPlace(0, y, "bottom")
                    translate([0, -trace_width / 2, 0])
                    cube([width_mm, trace_width, copper_width]);
            }
        }
        
        // perforations
        if (detailed)
        {
            material("green pcb")
            for (x = [0:width])
                for (y = [0:height])
                    translate([x * 2.54, y * 2.54, -1])
                    cylinder(pcb_thick + 2, d = drill);
        }
    }
}

// example
union()
{
    $fs = 0.5;
    PCB(10, 5);
    PCBPlace(1, 1)
    translate([0, 0, -2])
    cylinder(3, d=0.5);
}
