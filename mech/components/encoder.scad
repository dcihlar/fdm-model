use <../basic/material.scad>

module encoder(shaft="short", knob_distance=2)
{
    body_w = 10.92;
    body_d = 7.16;
    body_h = 10.16;
    pin_len = 3.68;
    pin_to_front = 3.96;
    top_to_shaft = 4.83;
    bot_to_shaft = body_h - top_to_shaft;
    shaft_len = (shaft == "short") ? 4.32 : 6.86;
    shaft_d = 3.56;
    
    // body
    translate([-body_w/2+2.54, -pin_to_front, 0])
    material("white plastic")
    cube([body_w, body_d, body_h]);
    
    // pins
    material("copper")
    for (x = [0:2])
    {
        for (y = [0:1])
        {
            translate([x * 2.54, y * 2.54, -pin_len/2])
            cube([0.51, 0.31, pin_len], center=true);
        }
    }
    
    // shaft
    material("black plastic")
    translate([2.54, -pin_to_front+1, bot_to_shaft])
    rotate([90, 0, 0])
    cylinder(shaft_len+1, d=shaft_d);
    
    // knob
    if (knob_distance >= 0 && knob_distance < 10)
    {
        material("black plastic")
        translate([2.54, -pin_to_front-knob_distance, bot_to_shaft])
        rotate([90, rands(10, 170, 1)[0], 0])
        difference()
        {
            cylinder(4.33, d=9.4);
            for (i = [0, 1])
            {
                mirror([0, i, 0])
                translate([-5, 4.83/2, 1.02])
                cube(10);
            }
            rotate([0, 0, 45])
            translate([2, 2, -1])
            cube(10);
        }
    }
}

// example
union()
{
    $fs=1;
    encoder();
}
