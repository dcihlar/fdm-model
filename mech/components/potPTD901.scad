use <../basic/material.scad>

module potPTD901()
{
    shaft_to_bot = 6.5;
    shaft_to_lock = 6.0;
    body_d = 7.05;
    body_w = 9.5;
    body_h = shaft_to_bot + 4.8;
    sbody_d = 0.3 * body_d;
    pbody_d = body_d - sbody_d;
    pin_thick = 0.3;
    pin_w = 0.8;
    pin_len = 3.5;
    pin_to_front = 5.0;
    lock_d = 0.8;
    lock_w = 2.0;
    lock_h = 0.8;
    bushing_len = 5;
    bushing_dia = 7;
    shaft_len = 15;
    shaft_dia = 6;
    
    // body
    translate([-body_w/2+2.54, -pin_to_front, 0])
    {
        material("blue plastic")
        translate([0, sbody_d, 0])
        cube([body_w, pbody_d, body_h]);
        material("steel")
        cube([body_w, sbody_d, body_h]);
    }
    
    // lock
    material("steel")
    translate([-lock_w/2+2.54, -pin_to_front-lock_d, shaft_to_bot-shaft_to_lock-lock_h/2])
    cube([lock_w, lock_d+1, lock_h]);
    
    // pins
    material("copper")
    for (i = [0:2])
    {
        translate([i * 2.54-pin_w/2, -pin_thick/2, -pin_len])
        cube([pin_w, pin_thick, pin_len+1]);
    }
    
    // bushing and shaft
    material("steel")
    translate([2.54, 1-pin_to_front, shaft_to_bot])
    rotate([90, 0, 0])
    {
        cylinder(bushing_len+1, d=bushing_dia);
        cylinder(shaft_len+1, d=shaft_dia);
    }
}


// example
union()
{
    $fs=0.5;
    potPTD901();
}
