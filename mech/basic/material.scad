materials = [
    ["black plastic", [0.30, 0.30, 0.30]],
    ["white plastic", [1.00, 1.00, 1.00]],
    ["blue plastic",  [0.04, 0.47, 0.75]],
    ["brass",         [0.87, 0.76, 0.15]],
    ["green pcb",     [0.20, 0.60, 0.20]],
    ["white pcb",     [1.00, 1.00, 1.00]],
    ["copper",        [1.00, 0.80, 0.40]],
    ["steel",         [0.80, 0.80, 0.80]],
    ["aluminum",      [0.60, 0.60, 0.60]],
    ["acrylic",       [0.80, 0.80, 1.00, 0.8]],
];

module material(name)
{
    material_id = search([name], materials)[0];
    material_color = materials[material_id][1];
    color(material_color)
    for (i = [0:$children-1])
        children(i);
}
