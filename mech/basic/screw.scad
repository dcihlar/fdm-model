use <../basic/material.scad>

module flatScrew(size, length, detailed=true)
{
    head_height = 0.8 * size;
    head_width = 1.8 * size;
    slot_size = 0.8 * size / 2.5;
    
    material("steel")
    {
        // head
        translate([0, 0, -head_height])
        difference()
        {
            cylinder(head_height, d=head_width);
            if (detailed)
            {
                rotate([0, 0, rands(10, 170, 1)[0]])
                cube([head_width+1,slot_size,slot_size], center=true);
            }
        }
        
        // body
        cylinder(length, d=size);
    }
}

module thumbScrew(size, length, detailed=true)
{
    head_height = 5.5 * size / 2.5;
    head_width  = 7 * size / 3.3;
    carving_height = 0.1 * head_height;
    carving_width  = 0.9 * head_width;
    embos_num = 10;
    embos_d = 1 * size / 2.5;
    embos_r = carving_width / 2;
    
    material("steel")
    {
        // head
        translate([0, 0, -head_height])
        if (detailed)
        {
            hull()
            {
                cylinder(head_height, d=carving_width);
                translate([0, 0, carving_height])
                cylinder(head_height - carving_height * 2, d=head_width);
            }
            
            for (i = [1:embos_num])
            {
                rotate([0, 0, i * 360 / embos_num])
                translate([embos_r, 0, carving_height])
                cylinder(head_height - carving_height * 2, d=embos_d);
            }
        }
        else
        {
            cylinder(head_height, d=head_width);
        }
        
        // body
        cylinder(length, d=size);
    }
}

// examples
union()
{
    $fs=0.1;
    translate([0,  0, 0]) flatScrew(2.5, 4);
    translate([0, 10, 0]) thumbScrew(2.5, 4, detailed=false);
}
