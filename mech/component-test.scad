use <components/encoder.scad>
use <components/pot3310C.scad>
use <components/potCA9.scad>
use <components/pcb.scad>
use <rack/card.scad>

$fs=0.4;
$fa=25;

card(25, 6, 10);
PCBPlace(8, 8, rot=180)
encoder(knob_distance=3);

PCBPlace(14, 8, rot=180)
pot3310C(knob_distance=3);

PCBPlace(19, 9, rot=180)
potCA9()
shaftCA9_9005();
